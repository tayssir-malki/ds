FROM java -jar /myproject-0.0.1-SNAPSHOT.jar

LABEL Name="ds" Version=1.4.2

ARG srcDir=src
WORKDIR /app
COPY $srcDir/requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY $srcDir/run.sh .
COPY $srcDir/ds ./ds

EXPOSE 5000

CMD ["gunicorn", "-b", "0.0.0.0:5000", "run:ds"]
